#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+----------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(IV|C) (32 bytes for SHA256) |
 * +------------+--------------------+----------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	if (entropy != NULL) {
		unsigned char* outBuf = malloc(KLEN_SKE * 2);

		// generating the aes and hmac keys and writing to outBuf
		// check for usage: https://www.openssl.org/docs/manmaster/crypto/HMAC.html
		HMAC(EVP_sha512(), KDF_KEY, strlen(KDF_KEY), entropy, entLen, outBuf,
				NULL);

		// set aes and hmac
		for (int i = 0; i < KLEN_SKE; i++) {
			K->aesKey[i] = outBuf[i];
			K->hmacKey[i] = outBuf[i + KLEN_SKE];
		}

		free(outBuf);
	} else {
		randBytes(K->aesKey, KLEN_SKE);
		randBytes(K->hmacKey, HM_LEN);
	}

	/* write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	// Randomize IV if null
	if (IV == NULL) {
		randBytes(IV, 16);
	}

	// Copy IV to first 16 bytes of outBuf
	memcpy(outBuf, IV, 16);

	// Encrypt
	int nWritten;

	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	// C = AES(plaintext) and copy C to outBuf + 16
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);
	if (1 != EVP_EncryptUpdate(ctx, outBuf + 16, &nWritten, inBuf,
					len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);

	size_t ctLen = 16 + nWritten + HM_LEN;

	// HMAC(IV|C) and copy value to outBuf + 16 + nWritten
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, outBuf, 16+nWritten, outBuf + 16 + nWritten,
				NULL);

	/* finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	return ctLen; /* should return number of bytes written, which
	             hopefully matches ske_getOutputLen(...). */
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
                        SKE_KEY *K, unsigned char *IV, size_t offset_out) {
    /* TODO: write this.  Hint: mmap. */
    int inFd = open(fnin, O_RDWR); // open that file right up
    struct stat sb;
    fstat(inFd, &sb); //get me some stats about that message file
    size_t len = (size_t) sb.st_size;
    char *mp = mmap(0, len, PROT_READ, MAP_PRIVATE, inFd, 0); //magic box to map file to virtual addresses
    if (mp == MAP_FAILED)
        printf("map failed");
    close(inFd);

    size_t ctLen = ske_getOutputLen(len); //steals from wes
    unsigned char *outBuf = malloc(ctLen);

    size_t cipherLen = ske_encrypt(outBuf, (unsigned char *) mp, len, K, IV); //put it in the cooker
    if (cipherLen == -1) {
        printf("encrypt returned -1");
    }
    munmap(mp, len); // deallocate


    int outFd = open(fnout, O_CREAT | O_RDWR);
    lseek(outFd, offset_out, SEEK_SET); //set file seek to offset_out

    ssize_t w = write(outFd, outBuf, cipherLen); //deliver that potato
    if (w == -1) {
        printf("write error, errorno %i\n", errno);
    }
    close(outFd);
    return cipherLen;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K) {
	unsigned char* H_MAC = malloc(HM_LEN);

	// HMAC(IV|C)
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, inBuf, len-HM_LEN, H_MAC,
				NULL);
	// Compare previous HMAC with current HMAC to see if errMask was applied on either IV, C or HMAC
	for (size_t i = 0; i < HM_LEN; i++) {
		if(H_MAC[i] != inBuf[(len - HM_LEN + i)]){
			// Ciphertext was tampered with
			free(H_MAC);
			return -1;
		}
	}
	free(H_MAC);

	unsigned char IV[16];

	// Copy first 16 bytes of inBuf to IV
	memcpy(IV, inBuf, 16);

	// Decrypt
	int nWritten = 0;
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	// plainText = AES(C) where C begins at inBuf + 16
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ctr(), 0, K->aesKey, IV))
		ERR_print_errors_fp(stderr);
	if (1 != EVP_DecryptUpdate(ctx, outBuf, &nWritten, inBuf+16, len - (16 + HM_LEN)))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);

	/* write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	return nWritten;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
	/* TODO: write this. */
    int inFd = open(fnin, O_RDWR); // open that file right up
    struct stat sb;
    fstat(inFd, &sb);
    size_t cipherTextLen = (size_t) sb.st_size;

    char *mp = mmap(0, cipherTextLen, PROT_READ, MAP_PRIVATE, inFd, 0); //magic box to map file to virtual addresses
    if (mp == MAP_FAILED)
        printf("map failed\n");
    close(inFd);

    unsigned char *plainTextBuf = malloc(cipherTextLen);
    size_t plainTextLen = ske_decrypt(plainTextBuf, (unsigned char *) mp, cipherTextLen, K); //put it in the cooker
    if (plainTextLen == -1) {
        printf("decrypt returned -1\n");
    }
    munmap(mp, cipherTextLen); // deallocate


    int outFd = open(fnout, O_CREAT | O_RDWR);
    lseek(outFd, offset_in, SEEK_SET); //set file seek to offset_in

    ssize_t w = write(outFd, plainTextBuf, plainTextLen); //deliver that potato
    if (w == -1) {
        printf("write error, errorno %i\n", errno);
    }
    close(outFd);


    return plainTextLen;
}
