#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "rsa.h"
#include "prf.h"

/* NOTE: a random composite surviving 10 Miller-Rabin tests is extremely
 * unlikely.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/
 * */
#define ISPRIME(x) mpz_probab_prime_p(x,10)
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

/* utility function for read/write mpz_t with streams: */
int zToFile(FILE* f, mpz_t x)
{
	size_t i,len = mpz_size(x)*sizeof(mp_limb_t);
	unsigned char* buf = malloc(len);
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b = (len >> 8*i) % 256;
		fwrite(&b,1,1,f);
	}
	Z2BYTES(buf,len,x);
	fwrite(buf,1,len,f);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}
int zFromFile(FILE* f, mpz_t x)
{
	size_t i,len=0;
	/* force little endian-ness: */
	for (i = 0; i < 8; i++) {
		unsigned char b;
		/* XXX error check this; return meaningful value. */
		fread(&b,1,1,f);
		len += (b << 8*i);
	}
	unsigned char* buf = malloc(len);
	fread(buf,1,len,f);
	BYTES2Z(x,buf,len);
	/* kill copy in buffer, in case this was sensitive: */
	memset(buf,0,len);
	free(buf);
	return 0;
}

int rsa_keyGen(size_t keyBits, RSA_KEY* K)
{
	size_t lenBytes = keyBits/8;
	rsa_initKey(K);

	unsigned char* outBuf = malloc(lenBytes);
	randBytes(outBuf, lenBytes);
	int count = 0;

	while (count < 2) {
		if (count == 0) {
			// Set p
			BYTES2Z(K->p, outBuf, lenBytes);
			
			// Check if p is prime
			if (ISPRIME(K->p)) {
				count++;
			}
		} else if (count == 1) {
			// Set q
			BYTES2Z(K->q, outBuf, lenBytes);

			// Check if q is prime
			if (ISPRIME(K->q)) {
				count++;
			}
		} 

		outBuf = malloc(lenBytes);
		randBytes(outBuf, lenBytes);
	}
	
	// Calculate n = p*q
	mpz_mul(K->n, K->p, K->q);

	// Initialize p_phi, q_phi, and n_phi
	NEWZ(p_phi);
	NEWZ(q_phi);
	NEWZ(n_phi);
	
	// Calculate Phi(n) as phi = (p-1)*(q-1)
	mpz_sub_ui(p_phi, K->p, 1);
	mpz_sub_ui(q_phi, K->q, 1);
	mpz_mul(n_phi, p_phi, q_phi);

	// Initialize flag to break out of while loop when e is found
	int flag = 0;
	while (!flag) {
		randBytes(outBuf, lenBytes);
		
		// Set e
		BYTES2Z(K->e, outBuf, lenBytes);

		// Initialize a and b for Euclidean Algorithm
		NEWZ(a);
		NEWZ(b);
		mpz_set(a, n_phi);
		mpz_set(b, K->e);

		while (mpz_cmp_ui(b, 1)){
			mpz_swap(a, b);
			mpz_mod(b, b, a);
			
			// Found e that is coprime with n_phi
			if (mpz_cmp_ui(b, 1) == 0){
				flag = 1;
			}
		}
		outBuf = malloc(lenBytes);
	}
	free(outBuf);

	// d * e = 1 mod phi_n
	mpz_invert(K->d, K->e, n_phi);

	/* write this.  Use the prf to get random byte strings of
	 * the right length, and then test for primality (see the ISPRIME
	 * macro above).  Once you've found the primes, set up the other
	 * pieces of the key ({en,de}crypting exponents, and n=pq). */
  
  return 0;
}

size_t rsa_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	// c = m^e mod n
	NEWZ(m);
	BYTES2Z(m, inBuf, len);
	NEWZ(c);
	mpz_powm(c, m, K->e, K->n);

	Z2BYTES(outBuf, len, c);
	/* write this.  Use BYTES2Z to get integers, and then
	 * Z2BYTES to write the output buffer. */
	return len; /* return should be # bytes written */
}
size_t rsa_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		RSA_KEY* K)
{
	// m = c^d mod n
	NEWZ(c);
	BYTES2Z(c, inBuf, len);
	NEWZ(m);
	mpz_powm(m, c, K->d, K->n);

	Z2BYTES(outBuf, len, m);
	/* write this.  See remarks above. */
	return len;
}

size_t rsa_numBytesN(RSA_KEY* K)
{
	return mpz_size(K->n) * sizeof(mp_limb_t);
}

int rsa_initKey(RSA_KEY* K)
{
	mpz_init(K->d); mpz_set_ui(K->d,0);
	mpz_init(K->e); mpz_set_ui(K->e,0);
	mpz_init(K->p); mpz_set_ui(K->p,0);
	mpz_init(K->q); mpz_set_ui(K->q,0);
	mpz_init(K->n); mpz_set_ui(K->n,0);
	return 0;
}

int rsa_writePublic(FILE* f, RSA_KEY* K)
{
	/* only write n,e */
	zToFile(f,K->n);
	zToFile(f,K->e);
	return 0;
}
int rsa_writePrivate(FILE* f, RSA_KEY* K)
{
	zToFile(f,K->n);
	zToFile(f,K->e);
	zToFile(f,K->p);
	zToFile(f,K->q);
	zToFile(f,K->d);
	return 0;
}
int rsa_readPublic(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K); /* will set all unused members to 0 */
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	return 0;
}
int rsa_readPrivate(FILE* f, RSA_KEY* K)
{
	rsa_initKey(K);
	zFromFile(f,K->n);
	zFromFile(f,K->e);
	zFromFile(f,K->p);
	zFromFile(f,K->q);
	zFromFile(f,K->d);
	return 0;
}
int rsa_shredKey(RSA_KEY* K)
{
	/* clear memory for key. */
	mpz_t* L[5] = {&K->d,&K->e,&K->n,&K->p,&K->q};
	size_t i;
	for (i = 0; i < 5; i++) {
		size_t nLimbs = mpz_size(*L[i]);
		if (nLimbs) {
			memset(mpz_limbs_write(*L[i],nLimbs),0,nLimbs*sizeof(mp_limb_t));
			mpz_clear(*L[i]);
		}
	}
	/* NOTE: a quick look at the gmp source reveals that the return of
	 * mpz_limbs_write is only different than the existing limbs when
	 * the number requested is larger than the allocation (which is
	 * of course larger than mpz_size(X)) */
	return 0;
}
